// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  hmr: false,
  api_url: 'https://uxcandy.com/~shapoval/',
  firebase: {
    apiKey: 'AIzaSyDzg4ANREkOrOnoxWZGUsHeyyGxS-uOMG0',
    authDomain: 'beejee-439ff.firebaseapp.com',
    databaseURL: 'https://beejee-439ff.firebaseio.com',
    projectId: 'beejee-439ff',
    storageBucket: 'beejee-439ff.appspot.com',
    messagingSenderId: '953169098207'
  }
};
