import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {PagerService} from '../../services/pager.service';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit {
  pager: any = {};
  @Output() setPage: EventEmitter<number> = new EventEmitter();

  constructor(private pagerService: PagerService) {
  }

  ngOnInit() {
  }

  setPageItems(page: number, totalItems: number, perPage?: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }
    this.pager = this.pagerService.getPager(totalItems, page, perPage);
  }

  setPages(number: number): void {
    this.setPage.emit(number);
  }

}
