import {Component, Input, OnInit} from '@angular/core';
import {Tasks, TasksService} from '../../../services/tasks.service';
import {AuthService} from '../../../core/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-task-item',
  templateUrl: './task-item.component.html',
  styleUrls: ['./task-item.component.css']
})
export class TaskItemComponent implements OnInit {
  @Input() task: Tasks;

  constructor(public auth: AuthService, private router: Router) {
  }

  ngOnInit() {
  }

  setEditTask() {
    localStorage.setItem('task', JSON.stringify(this.task));
    return this.router.navigate(['/tasks/edit']);
  }
}
