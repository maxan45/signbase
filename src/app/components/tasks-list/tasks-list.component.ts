import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {Tasks, TasksService} from '../../services/tasks.service';
import {PagerService} from '../../services/pager.service';
import {PaginationComponent} from '../pagination/pagination.component';

@Component({
  selector: 'app-tasks-list',
  templateUrl: './tasks-list.component.html',
  styleUrls: ['./tasks-list.component.css']
})
export class TasksListComponent implements OnInit {
  @Output() loading: EventEmitter<boolean> = new EventEmitter();
  @ViewChild(PaginationComponent) pagination: PaginationComponent;
  tasks: Tasks[];
  totalTasks: number;

  constructor(private tasksService: TasksService) {
  }

  ngOnInit() {
  }

  getTasks(page: number = 1): void {
    this.tasksService.getTasks(page)
      .subscribe(data => {
        this.loading.emit(false);

        this.tasks = data['message']['tasks'];
        this.totalTasks = data['message']['total_task_count'];
        this.pagination.setPageItems(page, this.totalTasks, 3);
      }, () => this.loading.emit(false));
  }
}
