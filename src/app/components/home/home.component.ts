import {Component, OnInit, ViewChild} from '@angular/core';
import {TasksListComponent} from '../tasks-list/tasks-list.component';
import {Tasks, TasksService} from '../../services/tasks.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  @ViewChild(TasksListComponent) tasksList: TasksListComponent;
  loading: boolean;

  constructor() {
  }

  ngOnInit() {
  }

  getTasks(): void {
    this.loading = true;
    this.tasksList.getTasks();
  }
}
