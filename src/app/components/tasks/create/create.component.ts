import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NotifyService} from '../../../core/notify.service';
import {TaskPreviewComponent} from './task-preview/task-preview.component';
import {TasksService} from '../../../services/tasks.service';
import {Router} from '@angular/router';
import {Ng2ImgToolsService} from 'ng2-img-tools';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  createTaskForm: FormGroup;
  file: File;
  imagePreview: any;
  notImage: boolean;
  @ViewChild(TaskPreviewComponent) taskPreview: TaskPreviewComponent;

  constructor(private fb: FormBuilder,
              public notifyService: NotifyService,
              private taskService: TasksService,
              private ng2ImgToolsService: Ng2ImgToolsService,
              private router: Router) {
  }

  ngOnInit() {
    this.createTaskForm = this.fb.group({
      'email': ['', [
        Validators.required,
        Validators.email
      ]
      ],
      'username': ['', [
        Validators.required
      ]
      ],
      'text': ['', [
        Validators.required
      ]
      ],
    });
  }

  createTask(): void {
    this.taskService.createTask(this.email.value, this.username.value, this.text.value, this.file)
      .subscribe(data => {
        if (data['status'] === 'ok') {
          return this.router.navigate(['/']);
        } else {
          this.notifyService.update(this.notifyService.handleCustomErros(data['message']), 'error');
        }
      });
  }

  createPreview(): void {
    this.taskPreview.createPreview(this.email.value, this.username.value, this.text.value, this.imagePreview);
  }

  /**
   *
   * @param event
   */
  fileChangeEvent(event: any): void {
    this.notImage = false;
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];
      if (file['type'] !== 'image/png' && file['type'] !== 'image/gif' && file['type'] !== 'image/jpg') {
        this.notImage = true;
        this.file = null;
        setTimeout(() => {
          this.notImage = false;
        }, 5000);
      } else {
        this.file = file;
        this.ng2ImgToolsService.resize([file], 320, 240).subscribe(result => {
          this.readURL(result);
        }, error => {
          console.log(error);
        });
      }
    }
  }

  /**
   * Preview image
   *
   * @param file
   */
  private readURL(file) {
    if (file) {
      const reader = new FileReader();
      reader.onload = (e) => {
        this.imagePreview = e.target['result'];
      };
      reader.readAsDataURL(file);
    }
  }

  /**
   *
   * @returns {AbstractControl | null}
   */
  get email() {
    return this.createTaskForm.get('email');
  }

  /**
   *
   * @returns {AbstractControl | null}
   */
  get username() {
    return this.createTaskForm.get('username');
  }

  /**
   *
   * @returns {AbstractControl | null}
   */
  get text() {
    return this.createTaskForm.get('text');
  }
}
