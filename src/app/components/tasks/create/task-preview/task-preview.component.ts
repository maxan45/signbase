import {Component, OnInit} from '@angular/core';
import {Tasks} from '../../../../services/tasks.service';
import {Subject} from 'rxjs/Subject';

@Component({
  selector: 'app-task-preview',
  templateUrl: './task-preview.component.html',
  styleUrls: ['./task-preview.component.css']
})
export class TaskPreviewComponent implements OnInit {
  private previewTask = new Subject<Tasks>();
  task = this.previewTask.asObservable();

  constructor() {
  }

  ngOnInit() {
  }

  createPreview(email, username, text, image: string) {
    const task: Tasks = new Tasks(email, username, text, image);
    this.previewTask.next(task);
  }
}
