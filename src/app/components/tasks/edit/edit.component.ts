import {Component, OnInit} from '@angular/core';
import {Tasks, TasksService} from '../../../services/tasks.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NotifyService} from '../../../core/notify.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  editTaskForm: FormGroup;
  task: Tasks;

  constructor(private fb: FormBuilder,
              private taskService: TasksService,
              private router: Router,
              public notifyService: NotifyService) {
    this.task = JSON.parse(localStorage.getItem('task'));
  }

  ngOnInit() {
    this.editTaskForm = this.fb.group({
      'text': ['', [
        Validators.required
      ]
      ],
      'status': ['', [
        Validators.required
      ]
      ]
    });
    this.editTaskForm.patchValue(this.task);
  }

  updateTask(): void {
    if (this.editTaskForm.valid) {
      this.taskService.updateTaskApi(this.task.id, this.text.value, +this.status.value)
        .subscribe((data) => {
          if (data['status'] === 'ok') {
            return this.router.navigate(['/']);
          } else {
            this.notifyService.update(this.notifyService.handleCustomErros(data['message']), 'error');
          }
        });
    }
  }

  /**
   *
   * @returns {AbstractControl | null}
   */
  get text() {
    return this.editTaskForm.get('text');
  }

  /**
   *
   * @returns {AbstractControl | null}
   */
  get status() {
    return this.editTaskForm.get('status');
  }
}
