import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../core/auth.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NotifyService} from '../../core/notify.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(public fb: FormBuilder, public auth: AuthService, public notifyService: NotifyService) {
  }

  ngOnInit() {
    this.loginForm = this.fb.group({
      'email': ['', [
        Validators.required,
        Validators.email
      ]
      ],
      'password': ['', [
        Validators.pattern('^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$'),
        Validators.minLength(6),
        Validators.maxLength(25),
        Validators.required
      ]
      ],
      'region': ['', []
      ],
    });
  }

  /**
   *
   * @returns {AbstractControl | null}
   */
  get email() {
    return this.loginForm.get('email');
  }

  /**
   *
   * @returns {AbstractControl | null}
   */
  get password() {
    return this.loginForm.get('password');
  }

  /**
   * Login
   *
   * @returns {Promise<void>}
   */
  login() {
    return this.auth.emailLogin(this.email.value, this.password.value);
  }
}
