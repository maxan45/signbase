import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../core/auth.service';
import {NotifyService} from '../../core/notify.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  registrationForm: FormGroup;
  detailForm: FormGroup;

  constructor(public fb: FormBuilder, public auth: AuthService, public notifyService: NotifyService) {
  }

  ngOnInit() {
    // First Step
    this.registrationForm = this.fb.group({
      'email': ['', [
        Validators.required,
        Validators.email,
      ],
      ],
      'password': ['', [
        Validators.pattern('^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$'),
        Validators.minLength(6),
        Validators.maxLength(25),
        Validators.required
      ]
      ],
      'region': ['', []
      ],
    });
    // Second Step
    this.detailForm = this.fb.group({
      'userType': ['', [Validators.required]]
    });
  }

  /**
   *
   * @returns {AbstractControl | null}
   */
  get email() {
    return this.registrationForm.get('email');
  }

  /**
   *
   * @returns {AbstractControl | null}
   */
  get password() {
    return this.registrationForm.get('password');
  }

  /**
   *
   * @returns {AbstractControl | null}
   */
  get userType() {
    return this.detailForm.get('userType');
  }

  /**
   * Login
   *
   * @returns {Promise<void>}
   */
  login() {
    return this.auth.emailSignUp(this.email.value, this.password.value);
  }

  /**
   *
   * @param user
   * @returns {Promise<void>}
   */
  setUserType(user) {
    return this.auth.updateUser(user, {userType: this.userType.value});
  }
}
