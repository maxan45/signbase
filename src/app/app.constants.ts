import {environment} from '../environments/environment';

const DEV_API = environment.api_url;

export const testTask = DEV_API + '/test-task-backend/';
export const createTask = DEV_API + '/test-task-backend/create';
export const editTask = DEV_API + '/test-task-backend/edit';
