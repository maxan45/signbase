import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import * as api from '../app.constants';
import {Subject} from 'rxjs/Subject';
import * as md5 from 'md5';

export class Tasks {
  id: number;
  username: string;
  email: string;
  text: string;
  status: number;
  image_path: string;

  constructor(email: string, username: string, text: string, image_path: string, status?: number, id?: number) {
    this.id = id || 0;
    this.email = email || '';
    this.username = username || '';
    this.text = text || '';
    this.image_path = image_path || 'http://saveabandonedbabies.org/wp-content/uploads/2015/08/default.png';
    this.status = status || 0;
  }
}

@Injectable()
export class TasksService {
  private editTask = new Subject<Tasks>();
  task = this.editTask.asObservable();

  constructor(private http: HttpClient) {

  }

  /**
   * Get tasks from API
   *
   * @param {number} page
   * @returns {Observable<any>}
   */
  getTasks(page?: number): Observable<any> {
    let Params = new HttpParams();
    Params = Params.append('developer', 'Maksym');
    Params = Params.append('page', page.toString());
    return this.http.get(`${api.testTask}`, {
      params: Params
    });
  }

  /**
   * Create task
   *
   * @param {string} email
   * @param {string} username
   * @param {string} text
   * @param {File} image
   * @returns {Observable<any>}
   */
  createTask(email: string, username: string, text: string, image: File): Observable<any> {
    const body: FormData = new FormData();
    if (image) {
      body.append('image', image, image.name);
    }
    body.append('email', email);
    body.append('username', username);
    body.append('text', text);
    return this.http.post(`${api.createTask}?developer=Maksym`, body);
  }

  /**
   *
   * @param {number} id
   * @param {string} text
   * @param {number} status
   * @returns {Observable<Object>}
   */
  updateTaskApi(id: number, text: string, status: number) {
    const body: FormData = new FormData();
    const token = 'beejee';
    const params: string[] = [
      `text=${text.replace(/\s/g, '')}`,
      `status=${status}`
    ].sort();
    params.push(`token=${token}`);
    params.join('&');
    const params_string = params.join('&');
    console.log('Params string ' + params_string);

    function fixedEncodeURIComponent(str) {
      return encodeURIComponent(str).replace(/[!'()*]/g, function (c) {
        return '%' + c.charCodeAt(0).toString(16);
      });
    }

    console.log('Encode URI ' + fixedEncodeURIComponent(params_string));
    const signature = md5(encodeURIComponent(params_string));
    console.log('SignatureL ' + signature);
    body.append('text', text);
    body.append('status', status.toString());
    body.append('signature', signature);
    return this.http.post(`${api.editTask}/${id}?developer=Maksym`, body);
  }
}
