import {NgModule} from '@angular/core';
import {TasksService} from './tasks.service';
import {PagerService} from './pager.service';

@NgModule({
  imports: [],
  providers: [TasksService, PagerService]
})
export class ServicesModule {
}
