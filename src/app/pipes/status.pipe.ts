import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'status'
})
export class StatusPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value === 0) {
      return 'task-not-done';
    } else if (value === 10) {
      return 'task-done';
    }
  }

}
