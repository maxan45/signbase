import {Component} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {LangService} from './core/lang.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private translate: TranslateService, private langService: LangService) {
    this.translate.setDefaultLang('en');
    this.translate.use(this.langService.lang);
  }

  title = 'app';
}

