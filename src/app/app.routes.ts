import {Routes} from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import {ProfileComponent} from './components/profile/profile.component';
import {AuthGuard} from './core/auth.guard';
import {RegistrationComponent} from './components/registration/registration.component';
import {HomeComponent} from './components/home/home.component';
import {CreateComponent} from './components/tasks/create/create.component';
import {EditComponent} from './components/tasks/edit/edit.component';

export const routes: Routes = [
  {
    path: '', component: HomeComponent
  },
  {
    path: 'login', component: LoginComponent
  },
  {
    path: 'tasks/create', component: CreateComponent
  },
  {
    path: 'tasks/edit', component: EditComponent, canActivate: [AuthGuard]
  },
  {
    path: 'registration', component: RegistrationComponent
  },
  {
    path: 'profile', component: ProfileComponent, canActivate: [AuthGuard]
  },
  {
    path: '',
    redirectTo: '',
    pathMatch: 'full'
  },
];
