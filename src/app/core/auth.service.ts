import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {AngularFireAuth} from 'angularfire2/auth';
import {AngularFirestore, AngularFirestoreDocument} from 'angularfire2/firestore';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/switchMap';
import {NotifyService} from './notify.service';

interface User {
  uid: string;
  email: string;
  photoURL?: string;
  displayName?: string;
  admin: boolean;
}

@Injectable()
export class AuthService {
  user: Observable<User>;

  constructor(private afAuth: AngularFireAuth,
              private afs: AngularFirestore,
              private notify: NotifyService,
              private router: Router) {
    //// Get auth data, then get firestore user document || null
    this.user = this.afAuth.authState
      .switchMap(user => {
        if (user) {
          return this.afs.doc<User>(`users/${user.uid}`).valueChanges();
        } else {
          return Observable.of(null);
        }
      });
  }

  /**
   * Email & password auth
   *
   * @param {string} email
   * @param {string} password
   * @returns {Promise<void>}
   */
  emailSignUp(email: string, password: string) {
    return this.afAuth.auth.createUserWithEmailAndPassword(email, password)
      .then(user => {
        return this.updateUserData(user); // create initial user document
      })
      .catch(error => this.handleError(error));
  }

  /**
   *
   * @param {string} email
   * @param {string} password
   * @returns {Promise<void>}
   */
  emailLogin(email: string, password: string) {
    return this.afAuth.auth.signInWithEmailAndPassword(email, password)
      .then(() => {
        this.router.navigate(['/profile']);
      })
      .catch(error => this.handleError(error));
  }

  /**
   * Update properties on the user document
   *
   * @param {User} user
   * @param data
   * @returns {Promise<void>}
   */
  updateUser(user: User, data: any) {
    return this.afs.doc(`users/${user.uid}`).update(data);
  }

  /**
   * Update user data
   *
   * @param user
   * @returns {Promise<void>}
   */
  private updateUserData(user) {
    // Sets user data to firestore on login
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);
    const data: User = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL,
      admin: false
    };
    return userRef.set(data);
  }

  /**
   * Logout
   */
  signOut() {
    this.afAuth.auth.signOut().then(() => {
      this.router.navigate(['/']);
    });
  }

  /**
   * If error console log and notify
   *
   * @param error
   */
  private handleError(error) {
    this.notify.update(error.code, 'error');
  }

}
