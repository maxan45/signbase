import {Injectable} from '@angular/core';

@Injectable()
export class LangService {
  /**
   * Language variable
   */
  private _lang: string;

  constructor() {
    this._lang = localStorage.getItem('lang');
    if (!this._lang) {
      localStorage.setItem('lang', 'en');
    }
  }

  get lang(): string {
    return this._lang;
  }
}
