import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';

/// Notify users about errors and other helpful stuff
export interface Msg {
  content: string;
  style: string;
}

@Injectable()
export class NotifyService {

  private _msgSource = new Subject<Msg>();

  msg = this._msgSource.asObservable();

  constructor() {
  }

  handleCustomErros(err: object): string[] {
    const errors = [];
    for (const item in err) {
      if (err.hasOwnProperty(item)) {
        errors.push(err[item]);
      }
    }
    return errors;
  }

  update(content: any, style: string) {
    const msg: Msg = {content, style};
    this._msgSource.next(msg);
  }

  clear() {
    this._msgSource.next(null);
  }

}
