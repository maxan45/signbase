import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';

import {environment} from '../environments/environment.hmr';
import {AngularFireModule} from 'angularfire2';

import {AppComponent} from './app.component';
import {CoreModule} from './core/core.module';
import {NavbarComponent} from './components/navbar/navbar.component';
import {RouterModule} from '@angular/router';
import {routes} from './app.routes';
import {LoginComponent} from './components/login/login.component';
import {ProfileComponent} from './components/profile/profile.component';
import {RegistrationComponent} from './components/registration/registration.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {ServicesModule} from './services/services.module';
import {TasksListComponent} from './components/tasks-list/tasks-list.component';
import {HomeComponent} from './components/home/home.component';
import {StatusPipe} from './pipes/status.pipe';
import {TaskItemComponent} from './components/tasks-list/task-item/task-item.component';
import {PaginationComponent} from './components/pagination/pagination.component';
import {CreateComponent} from './components/tasks/create/create.component';
import {EditComponent} from './components/tasks/edit/edit.component';
import {TaskPreviewComponent} from './components/tasks/create/task-preview/task-preview.component';
import {Ng2ImgToolsModule} from 'ng2-img-tools';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    ProfileComponent,
    RegistrationComponent,
    TasksListComponent,
    HomeComponent,
    StatusPipe,
    TaskItemComponent,
    PaginationComponent,
    CreateComponent,
    EditComponent,
    TaskPreviewComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    Ng2ImgToolsModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    AngularFireModule.initializeApp(environment.firebase),
    CoreModule,
    ServicesModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
